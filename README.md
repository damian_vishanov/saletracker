# Sale Tracker #

### Requirements ###
Create an extension to Business manager to reflect following reports.  
Consider There is special sale  going inside company.  
company wants to trackdown all the information and disclose to internal admin.  
  
what was it about,  
Person who leads the sales/ Channel manager  
when was it  
who participated (Customers),  
how much was the revenue  
Also company wants to keep track of information about the product that were sold within Demandware. Design a dashboard showing stats of all the products sold. An admin can go and add the product in the system and the product quantity can be decreased from Database inventory as soon as it is added to orders.  
  
System should leverage multiple currencies based on sales in different countries.  
In order to achieve the requirements if any integration needs to be added with external system (i.e. google analytics) that is in scope.  
  
Please use this private repository to version-control your code:  
Git/BitBucket  
Username: Your Username  
Password: Your Password  

### Implementation ###

* New cartridge named bm_saletracker has been created
* The project depends on the latest version of SFRA

### Screenshots ###
* Orders Page 1
![picture](screenshots/orders-page-1.png)
  
* Orders Page 2
![picture](screenshots/orders-page-2.png)
  
* Product Details on Sales Chart
![picture](screenshots/product-sales-chart.png)
  
* No data error handling
![picture](screenshots/no-data-found.png)
  
* Extension in the menu
![picture](screenshots/extension-in-the-menu.png)