'use strict';

function getCampaigns() {
    const PromotionMgr = require('dw/campaign/PromotionMgr');

    const campaigns = PromotionMgr.getCampaigns().toArray().filter(function (campaigns) {
        return campaigns.enabled;
    });

    return campaigns;
}

function getPromotions(campaignID) {
    const PromotionMgr = require('dw/campaign/PromotionMgr');
    const campaign = PromotionMgr.getCampaign(campaignID);
    let promotions = [];

    if (campaign) {
        promotions = campaign.getPromotions().toArray().filter(function (promotion) {
            return promotion.enabled;
        });
    }

    return promotions;
}

function getOrderPromotionInfo(order, promotionId) {
    return {
        isApplied: !empty(order.getPriceAdjustmentByPromotionID(promotionId))
    };
}

function getShippingPromotionInfo(order, promotionId) {
    return {
        isApplied: !empty(order.getShippingPriceAdjustmentByPromotionID(promotionId))
    };
}

function getProductPromotionInfo(order, promotionId) {
    const productLineItems = order.getProductLineItems();
    const products = [];
    let isApplied = false;

    if (!empty(productLineItems)) {
        productLineItems.toArray().forEach(function (lineItem) {
            if (!empty(lineItem.getPriceAdjustmentsByPromotionID(promotionId))) {
                isApplied = true;
                const pvm = lineItem.product.availabilityModel;
                let availability = pvm.orderable ? 1 : 0;

                if (pvm.inventoryRecord && pvm.inventoryRecord.ATS && pvm.inventoryRecord.ATS.value > 0) {
                    availability = pvm.inventoryRecord.ATS.value;
                }

                products.push({
                    ID: lineItem.getProductID(),
                    price: lineItem.adjustedGrossPrice,
                    qty: lineItem.getQuantityValue(),
                    availability: availability
                });
            }
        });
    }

    return {
        ID: promotionId,
        isApplied: isApplied,
        products: products
    };
}

function getPromotionInfo(order, promotion) {
    const Promotion = require('dw/campaign/Promotion');
    const promotionClass = promotion.getPromotionClass();

    if (promotionClass === Promotion.PROMOTION_CLASS_ORDER) {
        return getOrderPromotionInfo(order, promotion.ID);
    }

    if (promotionClass === Promotion.PROMOTION_CLASS_SHIPPING) {
        return getShippingPromotionInfo(order, promotion.ID);
    }

    return getProductPromotionInfo(order, promotion.ID);
}

function prepareOrderInfo(orderContainer) {
    const order = orderContainer.order;
    const Resource = require('dw/web/Resource');
    const customer = orderContainer.order.getCustomer();
    const promotions = [];
    let products = [];
    let customerFormatted = Resource.msg('report.customer.guest', 'saletracker', '');

    if (customer.registered) {
        customerFormatted = Resource.msgf(
            'report.customer.registered', 'saletracker',
            '',
            customer.profile.firstName,
            customer.profile.lastName,
            customer.profile.email
        );
    }

    orderContainer.promotions.forEach(function (promotion) {
        promotions.push(promotion.ID);

        if (!empty(promotion.products)) {
            products = products.concat(promotion.products.map(function (product) {
                return product.ID;
            }));
        }
    });

    return {
        ID: order.orderNo,
        date: order.creationDate,
        customer: customerFormatted,
        promotions: promotions,
        products: products,
        revenue: order.adjustedMerchandizeTotalGrossPrice
    };
}

function prepareOrders(campaignDetails) {
    const PropertyComparator = require('dw/util/PropertyComparator');
    const ArrayList = require('dw/util/ArrayList');
    const orders = new ArrayList(campaignDetails.orders.map(function (orderContainer) {
        return prepareOrderInfo(orderContainer);
    }));

    orders.sort(new PropertyComparator('date'));
    return orders;
}

function calculateProductSales(orders) {
    const HashMap = require('dw/util/HashMap');
    const productSales = new HashMap();

    orders.forEach(function (orderContainer) {
        orderContainer.promotions.forEach(function (promoInfo) {
            if (!empty(promoInfo.products)) {
                promoInfo.products.forEach(function (product) {
                    let productSale = productSales.get(product.ID);

                    if (productSale) {
                        productSale.revenue = productSale.revenue.add(product.price);
                        productSale.qty += product.qty;
                    } else {
                        productSale = {
                            productID: product.ID,
                            qty: product.qty,
                            revenue: product.price,
                            availability: product.availability
                        };
                    }

                    productSales.put(product.ID, productSale);
                });
            }
        });
    });

    return productSales.values().toArray();
}

function prepareChartDataPoints(campaignDetails) {
    const productSales = calculateProductSales(campaignDetails.orders);
    const chartDataPoints = [];
    productSales.forEach(function (productSale) {
        chartDataPoints.push({
            label: productSale.productID,
            y: productSale.qty,
            revenue: productSale.revenue.toFormattedString(),
            availability: productSale.availability.toFixed(0)
        });
    });

    return JSON.stringify(chartDataPoints);
}

function prepareCampaign(campaignDetails) {
    const HashMap = require('dw/util/HashMap');
    const campaign = campaignDetails.campaign;

    const revenueMap = new HashMap();

    campaignDetails.orders.forEach(function (orderContainer) {
        const order = orderContainer.order;
        const currencyCode = order.getCurrencyCode();
        const orderPrice = order.adjustedMerchandizeTotalGrossPrice;
        let revenue = revenueMap.get(currencyCode);

        if (revenue) {
            revenue = revenue.add(orderPrice);
        } else {
            revenue = orderPrice;
        }

        revenueMap.put(currencyCode, revenue);
    });

    const revenues = revenueMap.values().toArray().map(function (revenue) {
        return revenue.toFormattedString();
    });

    return {
        startDate: campaign.startDate,
        endDate: campaign.endDate,
        manager: session.userName,
        revenues: revenues
    };
}

function getOrdersPaging(orders, page) {
    const PAGE_SIZE = 10;
    const PagingModel = require('dw/web/PagingModel');
    const paging = new PagingModel(orders);
    const currentPage = page || 0;
    paging.setPageSize(PAGE_SIZE);
    paging.setStart(PAGE_SIZE * currentPage);
    return paging;
}

function getCampaignDetails(campaignID, promotionID) {
    const PromotionMgr = require('dw/campaign/PromotionMgr');
    const OrderMgr = require('dw/order/OrderMgr');
    const HashMap = require('dw/util/HashMap');
    const campaign = PromotionMgr.getCampaign(campaignID);

    if (empty(campaign)) {
        return null;
    }

    let promotions = campaign.getPromotions().toArray();

    if (promotionID) {
        promotions = promotions.filter(function (promotion) {
            return promotion.ID === promotionID;
        });
    }

    if (empty(promotions)) {
        return null;
    }

    const orders = OrderMgr.queryOrders('status != 6', null).asList().toArray();

    if (empty(orders)) {
        return null;
    }

    const ordersMap = new HashMap();

    promotions.forEach(function (promotion) {
        orders.forEach(function (order) {
            const promoInfo = getPromotionInfo(order, promotion);

            if (promoInfo.isApplied) {
                const orderNo = order.getOrderNo();
                let orderContainer = ordersMap.get(orderNo);

                if (orderContainer) {
                    orderContainer.promotions.push(promoInfo);
                } else {
                    orderContainer = {
                        order: order,
                        promotions: [promoInfo]
                    };
                }

                ordersMap.put(orderNo, orderContainer);
            }
        });
    });

    if (empty(ordersMap)) {
        return null;
    }

    return {
        campaign: campaign,
        orders: ordersMap.values().toArray()
    };
}

function getReportDetails(campaignID, promotionID, page) {
    const campaignDetails = getCampaignDetails(campaignID, promotionID);

    if (empty(campaignDetails)) {
        return null;
    }

    const orders = prepareOrders(campaignDetails);
    const URLUtils = require('dw/web/URLUtils');
    return {
        campaign: prepareCampaign(campaignDetails),
        ordersPaging: getOrdersPaging(orders, page),
        chartDataPointsJSON: prepareChartDataPoints(campaignDetails),
        pageParams: JSON.stringify({
            url: URLUtils.url('SaleTracker-GenerateReport').toString(),
            campaign: campaignID,
            promotion: promotionID
        })
    };
}

module.exports = {
    getCampaigns: getCampaigns,
    getPromotions: getPromotions,
    getReportDetails: getReportDetails
};
