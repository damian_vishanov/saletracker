$(function() {
    $('form.needs-validation').on('submit', function (e) {
        if ($(this).get(0).checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        }
        $(this).addClass('was-validated');
    });

    $('body').on('submit', '.saletTacker-form', function (e) {
        e.preventDefault();
        
        $.post($(this).attr('action'), $(this).serialize(), function (html) {
            $('.js-report-content').html(html);
        });
    });

    $('body').on('click', '.page-link', function (e) {
        e.preventDefault();
        
        var pageParams = $('.pagination').data('pageParams');
        pageParams.page = $(this).data('page');

        $.post(pageParams.url, $.param(pageParams), function (html) {
            $('.js-report-content').html(html);
        });
    });

    $('body').on('change', '#campaignFormField', function () {
        $.post($(this).data('getPromotionsUrl'), {
            campaign: $(this).val()
        }, function (html) {
            $('.js-promotions-select').html(html);
        });
    });
});
