$(function() {
    var dataPoints = $('.js-orders-table').data('chartDataPoints');
    var chart = new window.CanvasJS.Chart('chartContainer', {
        title: {
            text: 'Product Sales'
        },
        axisY: {
            title: 'Sold QTY'
        },
        data: [
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: 'column',
                toolTipContent: '{label}<br/>Sold QTY: {y}<br/>Revenue: {revenue}<br/>Availability: {availability}', 
                dataPoints: dataPoints
            }
        ]
    });
    chart.render();
});
