'use strict';

const server = require('server');
const saleTrackerHelper = require('~/cartridge/scripts/helpers/saleTracker');

server.get('Index', server.middleware.https, function (req, res, next) {
    res.render('saletracker/index');
    next();
});

server.get('Show', server.middleware.https, function (req, res, next) {
    const campaigns = saleTrackerHelper.getCampaigns();
    const promotions = saleTrackerHelper.getPromotions(campaigns[0] && campaigns[0].ID);
    res.render('saletracker/reportForm', {
        campaigns: campaigns,
        promotions: promotions
    });
    next();
});

server.post('GetPromotions', server.middleware.https, function (req, res, next) {
    res.render('saletracker/components/promotions', {
        promotions: saleTrackerHelper.getPromotions(req.form.campaign)
    });
    next();
});

server.post('GenerateReport', server.middleware.https, function (req, res, next) {
    let reportDetails;
    let error = false;

    try {
        reportDetails = saleTrackerHelper.getReportDetails(req.form.campaign, req.form.promotion, req.form.page);
    } catch (e) {
        error = true;
        const Logger = require('dw/system/Logger');
        Logger.error(e.fileName + ': ' + e.message + '\n' + e.stack);
    }

    if (error) {
        res.render('saletracker/components/error');
    } else if (reportDetails) {
        res.render('saletracker/reportDetails', reportDetails);
    } else {
        res.render('saletracker/components/noorders');
    }

    next();
});

module.exports = server.exports();
